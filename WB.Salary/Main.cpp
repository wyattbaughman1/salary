// Salary
// Wyatt Baughman

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct Record
{
	int ID;
	string FirstName;
	string LastName;
	double PayRate;
	int Hours;
};

int GetValues(double report[], int length)
{
	int total = 0;

	for (int i = 0; i < length; i++)
	{
		total += report[i];
	}

	return total;
}


int main()
{
	int num = 0;

	cout << "How Many Employees do you have: ";
	cin >> num;

	Record *employee;
	employee = new Record[num];

	int *ID = new int[num];
	string *FirstName = new string[num];
	string *LastName = new string[num];
	double *PayRate = new double[num];
	int *Hours = new int[num];
	double *GrossPay = new double[num];

	for (int i = 0; i < num; i++)
	{
		cout << "Enter the employee's ID: ";
		cin >> ID[i];
		cout << "Enter the employee's First Name: ";
		cin >> FirstName[i];
		cout << "Enter the employee's Last Name: ";
		cin >> LastName[i];
		cout << "Enter the employee's Pay Rate: ";
		cin >> PayRate[i];
		cout << "Enter the employee's Hours worked: ";
		cin >> Hours[i];
		GrossPay[i] = PayRate[i] * Hours[i];
	}

	for (int i = 0; i < num; i++)
	{
		employee[i].ID = ID[i];
		employee[i].FirstName = FirstName[i];
		employee[i].LastName = LastName[i];
		employee[i].PayRate = PayRate[i];
		employee[i].Hours = Hours[i];
	}

	cout << "\n" << "\n";

	for (int i = 0; i < num; i++)
	{
		cout << "Employee's ID: " << ID[i] << ", Name: " << FirstName[i] << " " << LastName[i] << "\n";
	}
	
	cout << "\n" << "\n";

	int total = GetValues(GrossPay, num);

	cout << "Total Gross Pay for all Employees: " << total;

	(void)_getch();
	return 0;
}
